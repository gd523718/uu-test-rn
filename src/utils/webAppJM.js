// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-nocheck
import uJm from 'crypto-js';
import JM from './jm_kf.js';
//生成前半部分加密key
const data = [
  'window',
  'const',
  'Number',
  'StringBuffer',
  'var',
  'let',
  'number',
  'Array',
  'Function',
  'String',
  'public',
  'string',
  'Object',
  'document',
  'BOM',
  'Boolean',
  'Object',
  'let',
  'Array',
  'function',
  'encypt',
  'decrypt',
  'solid',
];
let arr = JM.Enuu(
  Array.from(new Set(data)).filter((item, index) => {
    if (item.indexOf('s') > -1) {
      return item;
    }
  }),
);
let libs = arr.map((item, index) => {
  return item.slice(
    Math.floor(item.length / 2),
    Math.floor(item.length / 2) + 1,
  );
});
let str = '';
libs.forEach((item, index) => {
  str += item;
});
const webJm = {
  /**
   * 返回加密数据所需的前半部分key 后半部分为时间戳
   */
  getKey() {
    return str;
  },
  /**
   *@method 加密数据
   *@param {Object} data 需要加密的数据
   */
  JM(data) {
    let frontKey = this.getKey();
    let time = new Date().getTime().toString();
    let key = uJm.enc.Utf8.parse(frontKey + time);
    if (typeof data == 'object') {
      data = JSON.stringify(data);
    }
    let encryData = uJm.AES.encrypt(data, key, {
      iv: key,
      mode: uJm.mode.CBC,
      padding: uJm.pad.Pkcs7,
    }).toString();
    let decryData = uJm.AES.decrypt(encryData.toString(), key, {
      iv: key,
      mode: uJm.mode.CBC,
      padding: uJm.pad.Pkcs7,
    }).toString(uJm.enc.Utf8);
    return {
      data: encryData,
      time,
      decryData: JSON.parse(decryData),
    };
  },
};
export default webJm;
