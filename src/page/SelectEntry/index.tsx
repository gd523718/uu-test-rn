import {View, Text, Button, Alert} from 'react-native';
import React, {useState} from 'react';
import {ProfileScreenNavigationProp} from '@src/router/router';
import {$event} from '@src/utils/eventEmitter';
import {CouponItme} from '@src/types/Coupon';
type Props = {navigation: ProfileScreenNavigationProp};

const SelectEntry = (props: Props) => {
  const [couponInfo, setCouponInfo] = useState<CouponItme>();
  return (
    <View>
      <Button
        title="用户进入"
        onPress={() => {
          props.navigation.navigate('MyCoupon', {type: 'user'});
        }}
      />
      <Button
        title="商户进入"
        onPress={() => {
          props.navigation.navigate('MyCoupon', {type: 'merchant'});
        }}
      />
      <Button
        title="下单选择进入"
        onPress={() => {
          $event.add(
            'orderSelectionCoupon',
            (item: CouponItme) => setCouponInfo(item),
            true,
          );
          props.navigation.navigate('MyCoupon', {type: 'user', order: true});
        }}
      />
      <View>
        <Text>{JSON.stringify(couponInfo)}</Text>
      </View>
    </View>
  );
};

export default SelectEntry;
