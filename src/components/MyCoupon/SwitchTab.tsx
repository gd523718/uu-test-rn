import React, {FC, useState} from 'react';
import {
  View,
  TouchableWithoutFeedback,
  Text,
  Image,
  ImageStyle,
  StyleProp,
  StyleSheet,
  TextStyle,
  ViewStyle,
} from 'react-native';

type SwitchTabProps = {items: string[]; onChange: (index: number) => void};
const SwitchTab: FC<SwitchTabProps> = ({items, onChange}) => {
  const [active, setActive] = useState(0);
  const handleSwitch = (i: number) => {
    onChange(i);
    setActive(i);
  };
  return (
    <View style={styles.switchTab}>
      {items.map((item, i) => (
        <TouchableWithoutFeedback onPress={() => handleSwitch(i)} key={i}>
          <View style={styles.switchTab.itemBox}>
            <View style={styles.switchTab.item}>
              <Text
                style={[
                  styles.switchTab.itemText,
                  active === i ? styles.switchTab.itemActiveText : {},
                ]}>
                {item}
              </Text>
            </View>
            {active === i && (
              <Image
                style={styles.switchTab.itemIcon}
                source={require('@src/assets/img/MyCoupon/slices/slices.png')}
              />
            )}
          </View>
        </TouchableWithoutFeedback>
      ))}
    </View>
  );
};
export default SwitchTab;

export const styles = StyleSheet.create({
  switchTab: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    paddingHorizontal: 55,
    paddingVertical: 10,
    paddingBottom: 5,
    backgroundColor: '#fff',
    item: {
      width: 100,
      textAlign: 'center',
    } as StyleProp<ViewStyle>,
    itemBox: {
      position: 'relative',
      paddingBottom: 12,
    } as StyleProp<ViewStyle>,
    itemText: {
      textAlign: 'center',
      fontSize: 15,
      fontWeight: '500',
      color: '#999',
    } as StyleProp<TextStyle>,
    itemActiveText: {
      textAlign: 'center',
      fontSize: 15,
      fontWeight: '600',
      color: '#333',
    } as StyleProp<TextStyle>,
    itemIcon: {
      width: 21,
      position: 'absolute',
      bottom: 0,
      left: '50%',
      marginLeft: -10.5,
    } as StyleProp<ImageStyle>,
  },
});
