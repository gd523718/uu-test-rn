import {ProfileScreenNavigationProp} from '@src/router/router';
import React, {FC} from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';

interface FootProps {
  navigation: ProfileScreenNavigationProp;
}
const CouponFoot: FC<FootProps> = porops => {
  return (
    <View
      style={{
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: 10,
        paddingVertical: 20,
      }}>
      <View style={{flexDirection: 'row'}}>
        <Text style={{color: '#999', fontSize: 12}}>
          使用说明
          <AntDesign name="right" />
        </Text>
        <View
          style={{
            backgroundColor: '#D8D8D8',
            width: 1,
            height: 18,
            marginHorizontal: 13,
          }}>
          {}
        </View>
        <TouchableOpacity
          activeOpacity={0.7}
          onPress={() => {
            porops.navigation.navigate('HistoricalCoupons');
          }}>
          <Text style={{color: '#999', fontSize: 12}}>
            历史优惠券
            <AntDesign name="right" />
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};
export default CouponFoot;
