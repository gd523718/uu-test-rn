import {
  View,
  Text,
  StyleSheet,
  Image,
  StyleProp,
  ViewStyle,
  TextStyle,
  ColorValue,
  TouchableWithoutFeedback,
} from 'react-native';
import React, {FC, useContext} from 'react';
import {CouponItme} from '@src/types/Coupon';
import {CouponContext} from '@src/page/MyCoupon';

type Props = {
  item: CouponItme;
  /** 1 立即使用 2 已使用 3 已过期 | 4下单选择 */
  type: 1 | 2 | 3 | 4;
  onPress?: (item: CouponItme) => void;
  selectItem?: CouponItme;
};

const CouponItem: FC<Props> = props => {
  const {selectItem} = useContext(CouponContext);
  const {item} = props;
  const Operation = (p: {text: string; color?: ColorValue}) => {
    return (
      <>
        <View>
          <Image source={require('@src/assets/img/MyCoupon/line/Line.png')} />
        </View>
        <View style={styles.operation}>
          <Text style={[styles.operationT, p.color ? {color: p.color} : {}]}>
            {p.text}
          </Text>
        </View>
      </>
    );
  };
  const colorType =
    props.type === 2 || props.type === 3 ? {color: '#c1c1c1'} : {};

  return (
    <TouchableWithoutFeedback
      onPress={() => {
        props.onPress && props.onPress(props.item);
      }}>
      <View style={styles.couponItem}>
        <Image
          style={styles.bg}
          source={require('@src/assets/img/MyCoupon/itemBg/itemBg.png')}
        />
        {/* 过期时间判断 && 优惠券状态判断 */}
        {new Date().getTime() - 259200000 <
          new Date(item.ExpireDate.replace(/-/g, '/')).getTime() &&
          (props.type === 1 || props.type === 4) && (
            <View style={styles.tips}>
              <Text style={{fontSize: 11, color: '#FF2A1C', lineHeight: 20}}>
                即将到期
              </Text>
            </View>
          )}
        <View>
          <View style={styles.discount}>
            {item.DiscountType === '0' && (
              <Text
                style={[
                  styles.discount.discountText,
                  {fontSize: 20, right: 20},
                  colorType,
                ]}>
                ¥
              </Text>
            )}
            <Text style={[styles.discount.large, colorType]}>
              {item.Amount.split('.')[0]}
            </Text>
            {Boolean(item.Amount.split('.')[1]) && (
              <Text style={[styles.discount.decimal, colorType]}>
                .{item.Amount.split('.')[1]}
              </Text>
            )}
            {item.DiscountType === '1' && (
              <Text style={[styles.discount.discountText, colorType]}>折</Text>
            )}
          </View>
          {Boolean(item.CouponName) && (
            <View style={styles.condition}>
              {item.CouponName.split('$').map((t, i) => (
                <Text style={styles.conditionT} key={i}>
                  {t}
                </Text>
              ))}
            </View>
          )}
        </View>
        <View style={styles.textContent}>
          <Text
            style={[
              styles.title,
              props.type === 2 || props.type === 3 ? {color: '#c1c1c1'} : {},
            ]}>
            {item.CouponTitle}
          </Text>
          <View style={{paddingTop: 5}}>
            <Text style={styles.clause}>有效期至 {item.ExpireDate}</Text>
            <Text style={styles.clause}>{item.UseNote.split('($)')[0]}</Text>
            <Text style={styles.clause}>{item.UseNote.split('($)')[1]}</Text>
            <Text style={styles.clause}>{item.DiscountNote}</Text>
          </View>
        </View>
        {props.type !== 4 ? (
          <Operation
            text={
              props.type === 1
                ? '立即使用'
                : props.type === 2
                ? '已使用'
                : '已过期'
            }
            color={
              props.type === 1
                ? '#FF6900'
                : props.type === 2
                ? '#c1c1c1'
                : '#c1c1c1'
            }
          />
        ) : (
          <Image
            style={{width: 23}}
            source={
              item.CouponID === selectItem?.CouponID
                ? require('@src/assets/img/MyCoupon/checked/checked.png')
                : require('@src/assets/img/MyCoupon/Unchecked/Unchecked.png')
            }
          />
        )}
      </View>
    </TouchableWithoutFeedback>
  );
};

export default CouponItem;

const styles = StyleSheet.create({
  couponItem: {
    marginBottom: 10,
    backgroundColor: '#fff',
    borderWidth: 0.5,
    borderColor: '#E5E5E5',
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    minHeight: 130,
    borderRadius: 10,
    position: 'relative',
    padding: 12,
    paddingRight: 10,
  },
  discount: {
    flexDirection: 'row',
    color: '#FF6900',
    alignItems: 'flex-end',
    justifyContent: 'center',
    textAlign: 'center',
    paddingHorizontal: 18,
    minWidth: 100,
    decimal: {
      fontSize: 19,
      position: 'relative',
      bottom: 5,
      left: -1,
      color: '#FF6900',
    } as StyleProp<TextStyle>,
    large: {
      fontSize: 37,
      fontWeight: 'bold',
      color: '#FF6900',
    } as StyleProp<TextStyle>,
    discountText: {
      position: 'relative',
      bottom: 6,
      fontSize: 15,
      color: '#FF6900',
      left: 0,
      fontWeight: '500',
    } as TextStyle,
  },
  condition: {
    paddingTop: 5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  conditionT: {
    color: '#999',
    fontSize: 11,
  },
  tips: {
    position: 'absolute',
    top: 0,
    left: 0,
    backgroundColor: 'rgba(255,42,28, 0.12)',
    paddingHorizontal: 8,
    borderTopStartRadius: 10,
    borderBottomEndRadius: 10,
  },
  bg: {
    position: 'absolute',
    left: 0,
    bottom: 0,
  },
  operation: {
    paddingLeft: 11.5,
    width: 30,
  },
  operationT: {
    color: '#FF6900',
    fontSize: 15,
    fontWeight: '500',
    lineHeight: 20,
  },
  textContent: {
    flex: 1,
    marginRight: 10,
  },
  title: {
    lineHeight: 20,
    fontWeight: '500',
  },
  clause: {
    fontSize: 11,
    lineHeight: 14,
    color: '#999',
  },
});
