import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  TextStyle,
  TouchableWithoutFeedback,
} from 'react-native';
import React, {FC} from 'react';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import LinearGradient from 'react-native-linear-gradient';
import {CouponItme} from '@src/types/Coupon';
type Props = {
  selectItem?: CouponItme;
  confirm: (item?: CouponItme) => void;
};

const BottomButton: FC<Props> = props => {
  const insets = useSafeAreaInsets();
  const determine = () => {
    props.confirm(props?.selectItem);
  };
  return (
    <View style={[styles.BottomButton, {bottom: Number(insets.bottom) || 10}]}>
      <View style={styles.content}>
        <View style={styles.textBox}>
          <Text style={{fontSize: 15, color: '#fff'}}>已优惠</Text>
          <Text style={{fontSize: 15, color: '#FF6900', paddingLeft: 5}}>
            ¥{props?.selectItem?.Amount || 0}
          </Text>
        </View>
        <TouchableWithoutFeedback onPress={determine}>
          <LinearGradient
            start={{x: 0, y: 0}}
            end={{x: 1, y: 0}}
            style={styles.OKButton}
            colors={['#FF9111', '#FF752F']}>
            <View>
              <Text style={styles.OKButton.text}>确定</Text>
            </View>
          </LinearGradient>
        </TouchableWithoutFeedback>
      </View>
    </View>
  );
};

BottomButton.defaultProps = {};

export default BottomButton;

const styles = StyleSheet.create({
  BottomButton: {
    position: 'absolute',
    zIndex: 2,
    bottom: 0,
    width: Dimensions.get('window').width,
    paddingHorizontal: 10,
  },
  content: {
    height: 52,
    backgroundColor: '#333333',
    borderRadius: 52,
    overflow: 'hidden',
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
  },
  textBox: {
    flexDirection: 'row',
    paddingLeft: 20,
  },
  OKButton: {
    width: 104,
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    text: {
      color: '#FFFFFF',
      fontSize: 17,
      fontWeight: '500',
    } as TextStyle,
  },
});
