export interface ResponseBodyType<T = any> {
  Body?: T;
  Msg?: string;
  State?: number;
}
