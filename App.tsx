import React from 'react';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import {NavigationContainer} from '@react-navigation/native';
import {MyRouters} from './src/router/router';

const App = () => {
  return (
    <SafeAreaProvider>
      <NavigationContainer>
        <MyRouters />
      </NavigationContainer>
    </SafeAreaProvider>
  );
};

export default App;
