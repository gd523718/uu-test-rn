## 确保已安装环境所需依赖, 详细步骤见文档

<https://www.react-native.cn/docs/environment-setup/>

1. Xcode (Xcode Command Line Tools)
2. Node
3. Watchman
4. cocoapods

## 安装依赖

安装 node_modules 依赖

```bash
yarn
```

安装 ios pods 依赖

```bash
npx pod-install
```

## 启动 ios

yarn ios
